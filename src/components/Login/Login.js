import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginActions";
import styles from './Login.module.css'

function Login() {

    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.sessionReducer)

    const [ credentials, setCredentials ] = useState({
        username: ''
    })
    const [ isValid, setIsValid ] = useState(false)

    const onInputChange = event => {
        setCredentials({
            ...credentials,
            [event.target.id]: event.target.value
        })

        // if input = '', isValid is false and button is disabled.
        if (event.target.value === '') {
            setIsValid(false)
        } else {
            setIsValid(true)
        }

    }

    const onFormSubmit = event => {
        event.preventDefault() // stops the page from reloading.
        dispatch(loginAttemptAction(credentials))
    }


    return (
        <>
            {loggedIn && <Redirect to="/translation-page"/>} 
            {!loggedIn &&
            <AppContainer>
                <form className="mt-5 text-center" onSubmit={ onFormSubmit }>
                    <img className={ styles.logo } src="images/Logo-Hello.png" alt="Logo-Hello" />
                    <h2>Lost in Translation</h2>
                    <p>Get started</p>

                    <div className="input-group mb-3">
                        <input onChange={ onInputChange } id="username" type="text" className="form-control" placeholder="What's your name?"/>
                        <button disabled={!isValid} className="btn btn-outline-secondary" type="submit" >Go</button>
                    </div>
                </form>
            </AppContainer>
            }
        </>
    )
}

export default Login
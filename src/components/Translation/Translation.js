import { useSelector } from "react-redux"
import React from 'react'
import styles from './Translation.module.css'

function Translation() {
    const { inputText } = useSelector(state => state.translationReducer)

    return (
        <>
        <div className="card">

            <div className="card-header">
                {inputText}
            </div>
            <div className="card-body">
                {inputText.split('').map((letter, index) => {
                    if (letter === " ") { // spaces are returned as line breaks
                        return <br/>
                    }
                    return (
                        <img className={ styles.Signs } key={letter+index} src={ `images/${letter}.png` } alt={ letter }/>)
                })}
            </div>
        </div>
        </>
    )
}

export default Translation


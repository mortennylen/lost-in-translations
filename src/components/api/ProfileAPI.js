export const ProfileAPI = {
    logout(id) {
        return fetch(`http://localhost:4000/translations/${id}`, {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json'
            }
        })
        .then(async response => {
            return await response.json()
        })
    },


}
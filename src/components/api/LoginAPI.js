export const LoginAPI = {
    login(username) {
        return fetch('http://localhost:4000/user', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(username)
        })
        .then(async response => {
            return await response.json()
        })
    }

}
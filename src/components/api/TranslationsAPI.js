export const TranslationsAPI = {
    translate(translation) {
        return fetch('http://localhost:4000/translations', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(translation)
        })
        .then(async response => {
            return await response.json()
        })
    },

    getTranslations() {
        return fetch('http://localhost:4000/translations?deleted_ne=true')
        .then(async response => {
            return await response.json()
        })
    },

    markAsDeleted(id) {
        return fetch(`http://localhost:4000/translations/${id}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                deleted: true
                })
        })
        .then(async response => {
            return await response.json()
        })
    }
}
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { sessionLogoutAction } from "../../store/actions/sessionActions"
import { TranslationsAPI } from "../api/TranslationsAPI"
import { ProfileAPI } from "../api/ProfileAPI"
import styles from './Profile.module.css'

const Profile = () => {

    const dispatch = useDispatch()
    const { loggedIn } = useSelector(state => state.sessionReducer)

    const [ state, setState ] = useState({
        translations: [],
        fetching: true,
        error: ''
    })

    const [ isCleared, setIsCleared ] = useState(false)

    // Gets translations when component is rendered
    useEffect(() => {
        TranslationsAPI.getTranslations()
        .then(translations => {
            const reversedTranslations = translations.reverse() // reverses the translations array in order to easily access the latest translations
            setState({
                ...state,
                error: '',
                fetching: false,
                translations: reversedTranslations
                 
            })
        })
        .catch(error => {
            setState({
                ...state,
                error: error.message,
                fetching: false,
            })
        })
    }, [])

    // I could not figue out how to access nested objects with an endpoint, like http://localhost:4000/user/1/translations/,
    // or change mulitple objects with one request using the json-server.
    // Therefore I make an api request for every object i the 'translations' array, which seem like a bad solutions.
    // What is the best approach in this scenario? 
    const onLogoutClick = () => {
        for (const translation of state.translations) {
            ProfileAPI.logout(translation.id)
        }
        dispatch(sessionLogoutAction())
    }

    // Hides translations
    const onClearClick = () => {
        setIsCleared(true)
        for (const translation of state.translations) {
            TranslationsAPI.markAsDeleted(translation.id)
        }
    }
    
    return (
        <>
        <AppContainer>
           {!loggedIn && <Redirect to='/'/>}
                <header>
                    <div className="row">
                        <div className="col">
                            <img src="images/Logo-Hello.png" alt="Logo-Hello" width="25"/>
                            <span><Link to="/translation-page" style={{ textDecoration: 'none' }}>Lost in Translation</Link></span>
                        </div>
                        {/* Logout button */}
                        <div className="col text-end">
                            <button onClick={ onLogoutClick } className="btn btn-danger btn-sm">Logout</button>
                        </div>
                    </div>
                </header>
                
                <div className="card mt-4">
                    <div className="card-header">
                        {/* Clear button */}
                            <div className="col text-end">
                                <button onClick={ onClearClick } className="btn btn-outline-secondary btn-sm">Clear</button>
                            </div>
                    </div>

                    {!isCleared &&
                    <div className="card-body">
                        {state.translations.slice(0,10).map(translation => 
                            <p key={translation.id}>
                                <div className="row">
                                    {/* Translation (text) */}
                                    <div className="col">
                                        {translation.inputText}
                                    </div>
                                     {/* Translation (images) */}
                                    <div className="col">
                                        {translation.inputText.split('').map((letter, index) => 
                                            <img className={ styles.Signs } key={letter+index} src={ `images/${letter}.png` } alt={ letter }/>   
                                        )}
                                    </div>
                                </div>
                            </p> 
                        )}
                    </div>
                    }

                    <div className="card-footer">
                        Translations
                    </div>
                    
                </div>    
        </AppContainer>
       </>
    )
}

export default Profile
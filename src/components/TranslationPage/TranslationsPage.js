import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { translationSuccessAction } from "../../store/actions/translationActions"
import Translation from "../Translation/Translation"

const TranslationPage = () => {

    const dispatch = useDispatch()
    const { loggedIn, username } = useSelector(state => state.sessionReducer)

    const [ isValid, setIsValid ] = useState(true)
    const [ translation, setTranslation ] = useState({
        inputText: '',
        deleted: false
    })

    const onFormSubmit = event => {
        event.preventDefault() // stops the page from reloading.
        dispatch(translationSuccessAction(translation))
    }

    const onInputChange = event => {
        setTranslation({
            ...translation,
            [event.target.id]: event.target.value.trim(),
            
        })

        // If input is not a letter, isValid is false and error message renders and button is disabled.
        const regex = new RegExp('[^A-Za-z_ ]')
        if (regex.test(event.target.value)) {
            setIsValid(false)
        } else {
            setIsValid(true)
        }
    }
    

    return (
        <>
         <AppContainer>
            {!loggedIn && <Redirect to="/"/>}
                <header>
                    <div className="row">
                        <div className="col">
                            <img src="images/Logo-Hello.png" alt="Logo-Hello" width="25"/>
                            <span>Lost in Translation</span>
                        </div>
                        {/* Link to profile */}
                        <div className="col text-end">
                            <span>profile: <Link to="/profile">{username}</Link></span>
                        </div>
                    </div>
                </header>
                 
                <main className="mt-4">
                    <form onSubmit={ onFormSubmit }>
                        <div className="mb-4 input-group">
                            <input onChange={ onInputChange } type="text" id="inputText" className="form-control" placeholder="Write something..." maxLength="40"/>
                            <button disabled={!isValid} className="btn btn-outline-secondary" type="submit" >Translate</button>
                        </div>
                    </form>
                </main>

                {/* Error message box if input is not valid. */}
                {!isValid && 
                <div className="alert alert-danger" role="alert">
                    Input is not valid! Use letters only.
                </div>   
                }

                {/* Translation component displays the current translation.*/}
                <Translation/> 

         </AppContainer>
        </>
        
    )
}
export default TranslationPage
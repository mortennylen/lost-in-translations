import './App.css';
import{
  BrowserRouter,
  Switch,
  Route,
  //Redirect

} from 'react-router-dom';
//import AppContainer from './hoc/AppContainer';
import Login from './components/Login/Login';
import NotFound from './components/NotFound/NotFound';
import TranslationPage from './components/TranslationPage/TranslationsPage';
import Profile from './components/Profile/Profile';

function App() {
  return (
    <BrowserRouter>
      <div className="App">

        {/* <AppContainer>
        Lost In Translation
        </AppContainer> */}
        
        <Switch>
          <Route path="/" exact component={ Login }/>
          <Route path="/translation-page" component={ TranslationPage } />
          <Route path="/profile" component={ Profile } />
          <Route path="*" component={ NotFound }/>
        </Switch>
        
      </div>
    </BrowserRouter>
  );
}

export default App;

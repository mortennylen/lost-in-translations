import { ACTION_TRANSLATION_SUCCESS } from "../actions/translationActions"
import { ACTION_SESSION_LOGOUT} from "../actions/sessionActions"

const initialState = {
    inputText: "",
    deleted: false
}

export const translationReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_TRANSLATION_SUCCESS:
            return {
                ...action.payload
            }
        case ACTION_SESSION_LOGOUT:
            return {
                ...initialState
            }
        default:
            return state
    }
}
import { ACTION_SESSION_INIT, ACTION_SESSION_LOGOUT, ACTION_SESSION_SET, sessionSetAction } from "../actions/sessionActions"

export const sessionMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SESSION_INIT) {
        const storedSession = localStorage.getItem('rlit-ss')

        // If the storedSession does not exist. Breaks out/Returns out and does nothing.
        if (!storedSession) {
            return
        }
        // Parses "stringified session" back to JSON format.
        const session = JSON.parse(storedSession)

        dispatch(sessionSetAction(session))
        
    }

    if (action.type === ACTION_SESSION_SET) {
        // Saves the session to local storage. 
        localStorage.setItem('rlit-ss', JSON.stringify(action.payload.username)) //local storage can only store strings. Thus the object have to be stringified.
    }
    

    if (action.type === ACTION_SESSION_LOGOUT) {
        localStorage.clear()
    }

}

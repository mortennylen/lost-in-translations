import { TranslationsAPI } from "../../components/api/TranslationsAPI"
import { ACTION_TRANSLATION_POST, ACTION_TRANSLATION_SUCCESS, translationErrorAction, translationPostAction } from "../actions/translationActions"

export const translationMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_TRANSLATION_SUCCESS) {
        dispatch(translationPostAction(action.payload))
    }

    if (action.type === ACTION_TRANSLATION_POST) {
        try {
            TranslationsAPI.translate(action.payload)
        } catch (e) {
            dispatch(translationErrorAction(e.message))
        }  
    }
}

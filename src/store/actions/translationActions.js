export const ACTION_TRANSLATION_SUCCESS = '[translation] SUCCESS'
export const ACTION_TRANSLATION_POST = '[translation] POST'
export const ACTION_TRANSLATION_ERROR = '[translation] ERROR'

export const translationSuccessAction = translation => ({
    type: ACTION_TRANSLATION_SUCCESS,
    payload: translation
})

export const translationPostAction = translation => ({
    type: ACTION_TRANSLATION_POST,
    payload: translation
})

export const translationErrorAction = error => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: error
})


function AppContainer(props) {
    return (
        <div className="container">{props.children}</div>
    )
}

export default AppContainer

// //Using destructuring: 
// function AppContainer({ children }) {
//     return (
//         <div className="container">{children}</div>
//     )
// }

// export default AppContainer